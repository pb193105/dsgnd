var express = require('express'),
    ejs     = require('ejs'),
    bodyParser  = require("body-parser"),
    app     = express();

//Body Parser Config
app.use(bodyParser.urlencoded({extended: true}));
//View Engine ejs
app.set("view engine", "ejs");
app.set('views', __dirname+'/views');


app.get('/', function(req, res){
  res.render('login');
});




app.listen(3000, () => console.log('Example app listening on port 3000!'))
